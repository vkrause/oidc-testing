
#include <QtGui>
#include <QtCore>
#include <QtNetworkAuth>
#include <QSettings>
#include <QTimer>
#include <cstdio>

void doRequest(QOAuth2AuthorizationCodeFlow &oauth2)
{
    auto reply = oauth2.get(QUrl(QLatin1String("https://telemetry.kde.org/secure/")));
    QObject::connect(reply, &QNetworkReply::finished, &oauth2, [&oauth2, reply]() {
        reply->deleteLater();
        for (const auto &h : reply->request().rawHeaderList()) {
            qDebug() << h << ":" << reply->request().rawHeader(h);
        }
        qDebug() << QDateTime::currentDateTime() << oauth2.expirationAt() << reply->readAll() << reply->errorString();
        QTimer::singleShot(5* 60 * 1000, [&oauth2]() { doRequest(oauth2); });
    });
    QObject::connect(reply, &QNetworkReply::sslErrors, &oauth2, [reply](const auto &errors) {
        reply->ignoreSslErrors(errors); // self sign cert on the set setup
    });
}

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    QOAuth2AuthorizationCodeFlow oauth2;
    auto replyHandler = new QOAuthHttpServerReplyHandler(11450, &oauth2);
    oauth2.setClientIdentifier("<app id>");
    oauth2.setReplyHandler(replyHandler);
    oauth2.setAuthorizationUrl(QUrl("https://invent.kde.org/oauth/authorize"));
    oauth2.setAccessTokenUrl(QUrl("https://invent.kde.org/oauth/token"));
    oauth2.setScope("openid");

    QSettings settings;
    oauth2.setToken(settings.value("BearerToken", QString()).toString());
    oauth2.setRefreshToken(settings.value("RefreshToken", QString()).toString());

    QObject::connect(&oauth2, &QOAuth2AuthorizationCodeFlow::statusChanged, [&oauth2](QAbstractOAuth::Status status) {
        QSettings settings;
        settings.setValue("BearerToken", oauth2.token());
        settings.setValue("RefreshToken", oauth2.refreshToken());
        qDebug() << (int)status << oauth2.token() << oauth2.refreshToken() << oauth2.expirationAt() << oauth2.extraTokens();
        if (status == QAbstractOAuth::Status::Granted) {
            qDebug() << "authorization granted";
            doRequest(oauth2);
            QTimer::singleShot(std::max<qint64>(5 * 60 * 1000, QDateTime::currentDateTime().secsTo(oauth2.expirationAt()) * 800), &oauth2, &QOAuth2AuthorizationCodeFlow::refreshAccessToken);
        }
    });
    QObject::connect(&oauth2, &QOAuth2AuthorizationCodeFlow::authorizationCallbackReceived, [](const QVariantMap &m) {
        qDebug() << "auth callback received" << m;
    });
    QObject::connect(&oauth2, &QOAuth2AuthorizationCodeFlow::error, [](const QString &err, const QString &desc) {
        qDebug() << "error" << err << desc;
    });
    QObject::connect(&oauth2, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser, &QDesktopServices::openUrl);

    if (oauth2.refreshToken().isEmpty()) {
        oauth2.grant();
    } else if (!oauth2.expirationAt().isValid() || oauth2.expirationAt() < QDateTime::currentDateTimeUtc()) {
        oauth2.refreshAccessToken();
    }

    return app.exec();
}
