<?php

# request userinfo
$ch = curl_init('https://invent.kde.org/oauth/userinfo');
// TODO enforce SSL verification?
// TODO reuse the TCP connection for the next step?
$header = array();
$header[] = 'Authorization: Bearer ' . $_POST['token'];
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$userinfoData = curl_exec($ch);
if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) {
    // TODO error handling
    error_log('curl error while trying to obtain user info:' . curl_error($ch) . ' - ' . curl_getinfo($ch, CURLINFO_HTTP_CODE));
    return;
}
curl_close($ch);

# request introspect
$ch = curl_init('https://invent.kde.org/oauth/introspect');
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$introspectData = curl_exec($ch);
if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) {
    // TODO error handling
    error_log('curl error while trying to introspect:' . curl_error($ch) . ' - ' . curl_getinfo($ch, CURLINFO_HTTP_CODE));
    return;
}
curl_close($ch);

# merge results
$result = json_decode($userinfoData, true);
$introspect = json_decode($introspectData);
foreach ($introspect as $key => $value) {
    $result[$key] = $value;
}

print(json_encode($result));
?>
